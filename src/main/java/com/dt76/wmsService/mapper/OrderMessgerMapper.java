package com.dt76.wmsService.mapper;

import com.dt76.wmsService.utils.CommonReturnType;
import com.dt76.wmsService.utils.OrderMessage;
import com.dt76.wmsService.utils.OrderMessageShowUtil;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @Description
 * @auther jun
 * @create 2019-05-17 22:45
 */
public interface OrderMessgerMapper {


  /*  *//**
     * 下订单
     * @param orderShop
     * @return
     *//*
    CommonReturnType String(OrderMessage orderShop);
*/


    /**
     * 展示订单
     */
    List<OrderMessageShowUtil> findAllOrder(@Param("customerId") Long customerId,
                                            @Param("start")Integer start,
                                            @Param("size")Integer size);

    /**
     * 统计总数
     */

    Integer getOrderCount(@Param("customerId")Long customerId);


    /**
     * 查看订单详细信息
     */
    OrderMessageShowUtil findOrderMessage(@Param("orderId")String orderId);

    /**
     * 签收/订单退订
     */
    Integer updStatus(@Param("orderId")String orderId,
                      @Param("orderstatus")Long orderstatus);


}
