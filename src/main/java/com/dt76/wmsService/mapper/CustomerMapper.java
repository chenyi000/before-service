package com.dt76.wmsService.mapper;

import com.dt76.wmsService.pojo.SysCustomer;
import com.dt76.wmsService.utils.CustomerInfoUtil;
import org.apache.ibatis.annotations.Param;

/**
 * @Description
 * @auther jun
 * @create 2019-05-17 15:06
 */
public interface CustomerMapper {

    /**
     * 登录
     * @param loadname
     * @param password
     * @return
     */
    SysCustomer Login(@Param("loadname")String loadname,
                      @Param("password")String password);



    /**
     * 增加用户
     * @param sysCustomer
     * @return
     */
    Integer addCustomer(SysCustomer sysCustomer);

    /**
     * 删除
     * @param id
     * @return
     */
    Integer delCustomerById(@Param("id") Long id);


    /**
     * 修改
     * @param sysCustomer
     * @return
     */
    Integer updCustomer(SysCustomer sysCustomer);

    /**
     * 单个查找
     * @param id
     * @return
     */
    CustomerInfoUtil findCustomerById(@Param("id") Long id);



}
