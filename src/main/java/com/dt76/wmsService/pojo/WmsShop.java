package com.dt76.wmsService.pojo;


import lombok.Data;

@Data
public class WmsShop {

  private long id;
  private String createName;
  private java.util.Date createDate;
  private String updateName;
  private java.util.Date updateDate;
  private String shpName;
  private String shpType;
  private String shpCode;
  private String shpNum;
  private String shpWeight;
  private String inType;
  private String proDate;
  private String baozhiqi;
  private String unit;
  private long inDay;
  private String orderCode;
  private long statusId; //业务状态
  private java.util.Date chuhuoTime;
  private String shpTiji;


}
