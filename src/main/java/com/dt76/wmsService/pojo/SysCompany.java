package com.dt76.wmsService.pojo;


import lombok.Data;

import java.io.Serializable;

@Data
public class SysCompany implements Serializable {

  private long pId;
  private String pCode;
  private String pName;
  private String pPerson;
  private String pContacts;
  private String pTelphone;
  private String pEmail;
  private String pAddress;
  private String pCreatename;
  private java.util.Date pCreateTime;
  private String pUpdatename;
  private java.util.Date pUdateTIme;


}
