package com.dt76.wmsService.pojo;


import lombok.Data;

@Data
public class WmsOrder {

  private long id;
  private String createName;
  private java.util.Date createDate;
  private String updateName;
  private java.util.Date updateDate;
  private String cusCode;
  private String cusName;
  private String masName;
  private String cusAddress;
  private String masAddress;
  private String cusPhone;
  private String masPhone;
  private long shopId;
  private String typeId;


}
