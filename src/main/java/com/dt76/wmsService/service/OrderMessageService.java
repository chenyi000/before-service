package com.dt76.wmsService.service;

import com.dt76.wmsService.utils.OrderMessage;
import com.dt76.wmsService.utils.OrderMessageShowUtil;
import com.dt76.wmsService.utils.PageUitl;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Description
 * @auther jun
 * @create 2019-05-17 23:05
 */
public interface OrderMessageService {

    /**
     * 展示订单
     */
    PageUitl findAllOrder(Long customerId, Integer pageIndex);

    /**
     * 查看订单详细信息
     */
    OrderMessageShowUtil findOrderMessage(String orderId);

    /**
     * 订单退订
     */
    Integer updStatus(String orderId,Long status);

}
