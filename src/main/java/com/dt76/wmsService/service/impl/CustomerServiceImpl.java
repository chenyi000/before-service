package com.dt76.wmsService.service.impl;

import com.dt76.wmsService.mapper.CustomerMapper;
import com.dt76.wmsService.pojo.SysCustomer;
import com.dt76.wmsService.service.CustomerSerivce;
import com.dt76.wmsService.utils.CustomerInfoUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Description
 * @auther jun
 * @create 2019-05-17 15:25
 */

@Service
public class CustomerServiceImpl implements CustomerSerivce {

    @Autowired
    private CustomerMapper customerMapper;

    /**
     * 登录
     *
     * @param loadname
     * @param password
     * @return
     */
    @Override
    public SysCustomer Login(String loadname, String password) {
        return customerMapper.Login(loadname,password);
    }

    /**
     * 增加用户
     *
     * @param sysCustomer
     * @return
     */
    @Override
    public Integer addCustomer(SysCustomer sysCustomer) {
        return customerMapper.addCustomer(sysCustomer);
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    @Override
    public Integer delCustomerById(Long id) {
        return null;
    }

    /**
     * 修改
     *
     * @param sysCustomer
     * @return
     */
    @Override
    public Integer updCustomer(SysCustomer sysCustomer) {
        return null;
    }

    /**
     * 单个查找
     *
     * @param id
     * @return
     */
    @Override
    public CustomerInfoUtil findCustomerById(Long id) {
        return null;
    }
}
