package com.dt76.wmsService.service.impl;

import com.dt76.wmsService.mapper.OrderMessgerMapper;
import com.dt76.wmsService.service.OrderMessageService;
import com.dt76.wmsService.utils.OrderMessage;
import com.dt76.wmsService.utils.OrderMessageShowUtil;
import com.dt76.wmsService.utils.PageUitl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Description
 * @auther jun
 * @create 2019-05-17 23:05
 */

@Service
public class OrderMessageServiceImpl implements OrderMessageService {


    @Autowired
    private OrderMessgerMapper orderMessgerMapper;

    /**
     * 展示订单
     *
     * @param customerId
     */
    @Override
    public PageUitl findAllOrder(Long customerId,Integer pageIndex) {
        PageUitl pageUitl=new PageUitl();
        Integer orderCount = orderMessgerMapper.getOrderCount(customerId);
        Integer size=pageUitl.getPageSize();
        Integer pageNum=orderCount%size>0
                        ? orderCount/size+1
                        : orderCount/size;
        Integer start=(pageIndex-1)*size;
        List<OrderMessageShowUtil> allOrder = orderMessgerMapper.findAllOrder(customerId, start, size);

        pageUitl.setPageSumCount(orderCount);
        pageUitl.setPageNum(pageNum);
        pageUitl.setPageIndex(pageIndex);
        pageUitl.setData(allOrder);

        return pageUitl;
    }

    /**
     * 查看订单详细信息
     *
     * @param orderId
     */
    @Override
    public OrderMessageShowUtil findOrderMessage(String orderId) {
        OrderMessageShowUtil orderMessage = orderMessgerMapper.findOrderMessage(orderId);
        Integer status = orderMessage.getStatus();
        if(status < 2){
            orderMessage.setStatusStr("下单成功");
        }else if(status>=2 && status < 13 ){
            orderMessage.setStatusStr("配送中");
        }else if(status == 18 ){
            orderMessage.setStatusStr("已签收");
        }else if(status >=13 && status <18){
            orderMessage.setStatusStr("已退货");
        }else{
            orderMessage.setStatusStr("您的订单异常");
        }
        return orderMessage;
    }

    /**
     * 订单退订
     *
     * @param orderId
     * @param status
     */
    @Override
    public Integer updStatus(String orderId, Long status) {
        return orderMessgerMapper.updStatus(orderId,status);
    }
}
