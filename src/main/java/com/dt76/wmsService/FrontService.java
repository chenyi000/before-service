package com.dt76.wmsService;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
//启用服务的被注册中心发现--自己主动注册到注册中心去
@EnableDiscoveryClient
@MapperScan(basePackages = "com.dt76.wmsService.mapper")
public class FrontService {

    public static void main(String[] args) {
        SpringApplication.run(FrontService.class, args);
    }

}
