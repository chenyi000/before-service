package com.dt76.wmsService.controller;

import com.dt76.wmsService.pojo.SysCustomer;
import com.dt76.wmsService.service.CustomerSerivce;
import com.dt76.wmsService.utils.AccountUtil;
import com.dt76.wmsService.utils.CommonReturnType;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description
 * @auther jun
 * @create 2019-05-17 15:04
 */

@RestController
public class AccountController {

    @Autowired
    private CustomerSerivce customerSerivce;


    @RequestMapping("accountLogin2")
    public SysCustomer login(@RequestBody AccountUtil accountUtil){
        SysCustomer loginCustomer = customerSerivce.Login(accountUtil.getLoadname(),accountUtil.getPassword());
        return loginCustomer;
    }


    @RequestMapping("regiset2")
    public CommonReturnType regist(@RequestParam("syscustomer") String syscustomer){
        Gson gson=new Gson();
        SysCustomer sysCustomer = gson.fromJson(syscustomer, SysCustomer.class);
        Integer integer = customerSerivce.addCustomer(sysCustomer);
        if(integer > 0){
            return CommonReturnType.create("注册成功");
        }

        return CommonReturnType.create("注册失败","fail");
    }

}
