package com.dt76.wmsService.controller;

import com.dt76.wmsService.service.testservice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class testController {

    @Autowired
    private testservice testservice;

    @RequestMapping("/test")
    @ResponseBody
    public String Test(){
        return testservice.Test();
    }
}
