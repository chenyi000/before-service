package com.dt76.wmsService.controller;


import com.dt76.wmsService.service.OrderMessageService;
import com.dt76.wmsService.utils.CommonReturnType;
import com.dt76.wmsService.utils.OrderMessage;
import com.dt76.wmsService.utils.OrderMessageShowUtil;
import com.dt76.wmsService.utils.PageUitl;
import com.sun.org.apache.regexp.internal.RE;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @Description
 * @auther jun
 * @create 2019-05-17 22:45
 */

@RestController
@RequestMapping("/ordershop2")
public class OrderMessageController {

    @Autowired
    private OrderMessageService orderMessageService;

/*
    *//**
     * 展示订单
     *//*
    @RequestMapping("findOrder")
    public PageUitl findOrder(@RequestParam("customerid") Long customerId,
                              @RequestParam("pageIndex") String pageIndex) {
        return orderMessageService.findAllOrder(customerId, Integer.parseInt(pageIndex));
    }*/


    /**
     * 查看订单详细信息
     */
    @RequestMapping("findOrder")
    public OrderMessageShowUtil findOrderMessage(@RequestParam("orderId") String orderId) {
        return orderMessageService.findOrderMessage(orderId);
    }



    /**
     * 签收/订单退订
     */
    @RequestMapping("/accptOrQuitOrder")
    public CommonReturnType accptOrQuitOrder(@RequestParam("orderId") String orderId,
                                             @RequestParam("status") String status) {
        Integer result =0;
            result=orderMessageService.updStatus(orderId, Long.parseLong(status));
        if (result != null) {
            return CommonReturnType.create("操作成功");
        }
        return CommonReturnType.create("操作失败", "fail");
    }
}

